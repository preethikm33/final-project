import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit
{
  id!:number;
  user:User=new User();

  constructor(private userService:UserService,private router:Router)
  {}
  
  
  
  
  
  ngOnInit(): void {
   this.userService.getUserById(this.id).subscribe(data=>{
    this.user=data;
   }),
     (   error: any)=>console.log(error);
  }

  onSubmit()
  {
    this.userService.updateUser(this.id,this.user).subscribe(data=>{
      this.goToUserList();
    })
  }
goToUserList()
{
this.router.navigate(['/users']);
}
}
